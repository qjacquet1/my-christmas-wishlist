
export const environment = {
    production: false,

    api: {
        isFakeBackend: true,
        url: 'localhost',
    },
};
