import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { Subject } from 'rxjs';
import { Config, appConfig } from '@core/config/config';
import { MatSidenav } from '@angular/material';
import { User } from '@core/models/user';
import { AuthenticationService } from '@core/api/authentication/authentication.service';
import { SidebarService } from './layout/sidebar/sidebar.service';
import { SplashScreenService } from './layout/splash-screen/splash-screen.service';
import { animations } from '@core/config/animations';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations
})
export class AppComponent implements OnInit {

    unsubscribeAll: Subject<any>;

    config: Config;

    @ViewChild('sidenav', null) public sidenav: MatSidenav;

    currentUser: User;

    constructor(
        private authenticationService: AuthenticationService,
        private sidebarService: SidebarService,
        private splashScreenService: SplashScreenService,
    ) {
        // Set the private defaults
        this.unsubscribeAll = new Subject();

        // Get current user
        this.authenticationService.currentUser.subscribe(u => {
            this.currentUser = u;
        });

        this.config = appConfig;
    }

    ngOnInit(): void {

        // Locale
        this.initLocale();

        // Sidenav
        this.initSidenav();

        // Splash screen disable
        this.initSplashscreenDisable();
    }

    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    }

    initLocale() {
        registerLocaleData(localeFr);
    }

    initSidenav() {
        this.sidebarService.setSidenav(this.sidenav);
        this.config.sidebar.opened ? this.sidebarService.open() : this.sidebarService.close();
    }

    initSplashscreenDisable() {
        this.splashScreenService.hide();
    }
}
