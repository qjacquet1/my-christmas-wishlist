import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@core/shared/shared.module';

import { ListComponent } from './pages/list/list.component';
import { ListItemComponent } from './components/list-item/list-item.component';

const routes: Routes = [
  {
      path     : '',
      component: ListComponent,
  }
];

@NgModule({
  declarations: [
      ListComponent,
      ListItemComponent
    ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,

    SharedModule
  ],
})
export class WishlistsModule { }
