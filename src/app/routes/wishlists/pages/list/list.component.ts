import { Component, OnInit } from '@angular/core';
import { Wishlist } from '@core/models/wishlist';
import { WishlistsModule } from 'routes/wishlists/wishlists.module';

@Component({
  selector: 'wishlists-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

    wishlists: Wishlist[];

    ngOnInit(): void {
        this.wishlists = [
            {
                id: 1,
                userId: 1,
                title: 'Ma liste Apple',
                items: [
                    {
                        title: 'iPhone 11',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'iPad',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'Macbook',
                        isOwned: false,
                        ownerId: null,
                    },
                ]
            },
            {
                id: 2,
                userId: 1,
                title: 'Ma liste random',
                items: [
                    {
                        title: 'Objet 1',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'Objet 2',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'Objet 3',
                        isOwned: false,
                        ownerId: null,
                    },
                ]
            },
            {
                id: 3,
                userId: 2,
                title: 'Ma liste Apple',
                items: [
                    {
                        title: 'iPhone 11',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'iPad',
                        isOwned: false,
                        ownerId: null,
                    },
                    {
                        title: 'Macbook',
                        isOwned: false,
                        ownerId: null,
                    },
                ]
            },
        ];
    }
}
