import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ItemFormComponent } from '../item-form/item-form.component';
import { Wishlist } from '@core/models/wishlist';

@Component({
    selector: 'wishlist-list-item',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

    constructor(
        public dialog: MatDialog
    ) { }

    @Input() data: Wishlist;

    ngOnInit() {
    }

    openDialog() {
        const dialogRef = this.dialog.open(ItemFormComponent, {
            width: '250px',
            data: this.data
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.data = result;
        });
    }

}
