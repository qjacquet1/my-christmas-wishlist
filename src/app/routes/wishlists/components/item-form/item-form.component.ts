import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Wishlist } from '@core/models/wishlist';

@Component({
    selector: 'wishlist-item-form',
    templateUrl: './item-form.component.html',
})
export class ItemFormComponent {

    constructor(
        public dialogRef: MatDialogRef<ItemFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Wishlist) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
