import { AlertService } from '@core/components/alert/alert.service';
import { AuthenticationService } from '../../../../core/api/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
    ) {
        // // redirect to home if already logged in
        // if (this.authenticationService.isLogged()) {
        //     this.router.navigate(['/']);
        // }
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            firstName: ['', null],
            lastName: ['', null],
            email: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.register(this.getFormUser())
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.show('Registration successful');
                    this.router.navigate(['/']);
                },
                error => {
                    this.alertService.show(error);
                    this.loading = false;
                });
    }

    getFormUser() {
        const name = this.registerForm.get('name').value.split(' ', 2);

        return new Object({
            username: this.registerForm.get('email').value,
            password: this.registerForm.get('password').value,
            firstName: name[0],
            lastName: name[1]
        });
    }
}
