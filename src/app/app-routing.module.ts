import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/helpers/auth.guard';

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: './routes/authentication/authentication.module#AuthenticationModule'
    },
    {
        path: '',
        loadChildren: './routes/wishlists/wishlists.module#WishlistsModule',
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
      AuthGuard
    ]
  })
export class AppRoutingModule { }
