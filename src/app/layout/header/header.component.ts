import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@core/api/authentication/authentication.service';
import { User } from '@core/models/user';
import { SidebarService } from '../sidebar/sidebar.service';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { AlertService } from '@core/components/alert/alert.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private sidenav: SidebarService,
    private overlay: Overlay,
    private alertService: AlertService
  ) { }

  @Input() title: string;
  @Input() currentUser: User;

  overlayRef: OverlayRef;

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['auth/login']);
  }

  toggleSidenav() {
    this.sidenav.toggle();
  }

  isOpenSidenav() {
    return this.sidenav.isOpen();
  }

  showSidenav() {
    this.sidenav.open();
  }

}
