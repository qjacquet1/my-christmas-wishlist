import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@core/shared/shared.module';

import { SidebarComponent } from './sidebar.component';
import { NavCollapsableComponent } from './sidenav/collapsable/collapsable.component';
import { NavGroupComponent } from './sidenav/group/group.component';
import { NavItemComponent } from './sidenav/item/item.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    SidebarComponent,
    NavCollapsableComponent,
    NavGroupComponent,
    NavItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  exports: [SidebarComponent]
})
export class SidebarModule { }
