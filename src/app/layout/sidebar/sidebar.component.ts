import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { navigation, Navigation } from '@core/config/navigation';
import { SidebarService } from './sidebar.service';
import { Config } from '@core/config/config';

import { MatDialog } from '@angular/material';
import { AlertService } from '@core/components/alert/alert.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  items: Navigation[];

  @Input() config: Config;

  constructor(
    private sideNavService: SidebarService,
    public dialog: MatDialog,
    public alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.items = navigation;
  }

  hide() {
    this.sideNavService.close();
  }

  disconnect() {
    //this.authenticationService.logout();
  }

}
