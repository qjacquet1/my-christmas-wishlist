import { NgModule } from '@angular/core';

import { MatElevationDirective } from './mat-elevation-directive';

@NgModule({
    declarations: [
        MatElevationDirective,
    ],
    imports     : [
    ],
    exports     : [
        MatElevationDirective,
    ],
    entryComponents: [
    ]
})
export class DirectivesModule {
}
