export interface Wishlist {
    id: number;
    userId: number;
    title: string;
    items?: Item[];
}

interface Item {
    title?: string;
    isOwned?: boolean;
    ownerId?: number;
}
