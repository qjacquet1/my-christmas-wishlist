import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@core/shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SidebarModule } from './layout/sidebar/sidebar.module';
import { AuthenticationService } from '@core/api/authentication/authentication.service';
import { JwtInterceptor } from '@core/helpers/jwt.interceptor';
import { ErrorInterceptor } from '@core/helpers/error.interceptor';
import { MAT_DATE_LOCALE } from '@angular/material';
import { environment } from 'environment/environment';
import { FakeBackendProvider, BackendProvider } from '@core/helpers/fake-backend';
import { AlertService } from '@core/components/alert/alert.service';
import { SidebarService } from './layout/sidebar/sidebar.service';
import { HeaderComponent } from './layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    SidebarModule,
  ],
  providers: [
    AuthenticationService,

    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: LOCALE_ID},

    // provider used to create fake backend
    environment.api.isFakeBackend ? FakeBackendProvider : BackendProvider,

    AlertService,
    SidebarService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
